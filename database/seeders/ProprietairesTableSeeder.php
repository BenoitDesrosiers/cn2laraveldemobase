<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Proprietaire;
use App\Models\Animal;
use Illuminate\Support\Facades\DB;

class ProprietairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //commence par effacer la table de relation afin de ne pas avoir de problème avec les FK
        DB::table('animaux_proprietaires')->delete();

        DB::table('proprietaires')->delete();

        $timinou = Animal::where('nom', '=', 'ti-minou')->first();
        $killer = Animal::where('nom', '=', 'killer')->first();
        $papoute = Animal::where('nom', '=', 'papoute')->first();
        $chouka = Animal::where('nom', '=', 'chouka')->first();

        $p = new Proprietaire();
        $p->nom = "Benoit";
        $p->telephone = "123-123-1234";
        $p->save();

        //Exemple de many to many en partant du proprietaire
        $p->animaux()->attach($timinou->id);
        $p->animaux()->attach($papoute->id);


        $p = new Proprietaire();
        $p->nom = "George";
        $p->telephone = "456-123-1234";
        $p->save();

        //Exemple de many to many en partant de l'animal
        //important de tester les deux options afin de vérifier que autant animaux() que proprietaires() soient bien définis.
        $killer->proprietaires()->attach($p);
        $chouka->proprietaires()->attach($p);


    }
}
