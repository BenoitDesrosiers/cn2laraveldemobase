<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Animal;
use App\Models\Espece;
use Illuminate\Support\Facades\DB;

class AnimauxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //commence par effacer la table de relation afin de ne pas avoir de problème avec les FK
        DB::table('animaux_proprietaires')->delete();

        DB::table('Animaux')->delete();

        $chat = Espece::where('nom', '=', 'chat')->first();
        $chien = Espece::where('nom', '=', 'chien')->first();

        $a = new Animal();
        $a->nom = "ti-minou";
        //Exemple de one-to-many
        $chat->animaux()->save($a);
        $a = new Animal();
        $a->nom = "killer";
        $chat->animaux()->save($a);

        $a = new Animal();
        $a->nom = "papoute";
        //Exemple d'une association one-to-many (hasMany)
        $chien->animaux()->save($a);

        $a = new Animal();
        $a->nom = "chouka";
        //Exemple d'une association many-to-one (belongsTo)
        $a->espece()->associate($chien);
        $a->save();

        //$chien->animaux()->save($a);
    }
}
