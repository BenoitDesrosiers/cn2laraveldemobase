<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Models\Espece;
class EspecesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('especes')->delete();

        $espece = new Espece();
        $espece->nom = "chat";
        $espece->save();

        $espece = new Espece();
        $espece->nom = "chien";
        $espece->save();

    }
}
