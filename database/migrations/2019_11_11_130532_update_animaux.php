<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAnimaux extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Exemple d'un changement dans un migration.
        //Une fois qu'une migration est pushed sur le repos GIT, il est important de ne plus la modifier
        //car le système de migration retient quelles migrations ont été faites.
        //Si une erreur est trouvée dans une migration, il faut en faire une autre pour corriger cette erreur.

        //Exemple d'un up qui ajoute une FK

        //Ajout de la FK
        Schema::table('animaux', function (Blueprint $table) {
            $table->foreign('espece_id')
                ->references('id')->on('especes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Exemple du down qui enlève une FK.

        Schema::table('animaux', function (Blueprint $table) {
            $table->dropForeign(['espece_id']);
        });

    }
}
