<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
 * ATTENTION à partir de Laravel 8.0, il y a un changement dans le routing qui peut faire que les views ne
 * fonctionnent plus correctement lors de l'appel de "action"
 * voir https://laravel.com/docs/8.x/upgrade#routing
 * Par exemple, cette ligne dans layout.blade.php ne fonctionnera pas si le namespace n'est pas rétablie
 * <a href="{!! action('ProprietairesController@index') !!}">
 */
Route::resource('proprietaires', 'ProprietairesController');
Route::resource('animaux', 'AnimauxController');
