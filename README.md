# CN2LaravelDemoBase
> Un projet simple démontrant comment utiliser Laravel 6.1 ainsi que VUE.js. Créé dans le cadre du cours 420-CN2-DM Web 2 à l'automne 2019. 


## Table des matières
* [Info générale](#info-générale)
* [Technologies](#technologies)
* [Installation](#installation)
* [Fonctionnalités](#fonctionnalités)
* [Status](#status)
* [Contact](#contact)

## Info générale
Ce projet est à but purement pédagogique. Il permet de démontrer comment batir un système de base en utilisant Laravel. 
Il se compose de 3 classes: Les espèces, les animaux, et les propriétaires

Dans le cadre de mon cours, je devais présenter
aux étudiantes et étudiants les fonctionnalités de base permettant de créer un CRUD simple sans relation (les espèces (non fait à date)) 
un CRUD ayant une relation 1 à plusieurs (les animaux qui sont reliés à leur espèce), et une relation plusieurs à plusieurs 
(les propriétaires ayant plusieurs animaux qui peuvent appartenir à plusieurs proprios) 

Ces exemples en Laravel se trouvent sur la branche master. 

Dans un deuxième temps, je désirais présenter comment réaliser les mêmes fonctionnalités en utilisant VUE js. Afin de garder les deux exemples séparés, le code VUE se trouve sur la branche demoVueCRUD



## Technologies
*  Laravel 6.1 (utilise PHP 7.2)
*  VUE.js 2.5.17

Des plugins supplémentaires ont été utilisés pour la partie VUE.
*  vue-router 3.0.1
*  axios 0.19 (oui, faut j'upgrade)
*  sweetalert2 7.33.1


## Installation
Ce guide d'installation n'est pas complet. Il présume que vous avez déjà installé un projet Laravel (ou que vous avez un professeur qui vous aidera :) ) 
### Préalables


* Vous devez avoir PHP 7.2 ainsi qu'un serveur MySQL/MariaDB. Le plus simple est d'installer XAMPP (ou tout autre pile équivalente)
* Vous devez avoir Laravel d'installé. Si ce n'est pas fait, suivre les instructions sur https://laravel.com/docs/6.x

### Téléchargement du projet
Téléchargez le projet à partir de https://gitlab.com/BenoitDesrosiers/cn2laraveldemobase.git



### Démarrage de la version purement en Laravel 

Si vous désirez le démo purement en Laravel: 
```
git checkout master
```


Copiez le fichier .env.example dans .env et modifiez la ligne **DB_DATABASE=Animaux** ainsi que toute autre ligne nécessaire pour votre environnement. 

Créez la base de données **Animaux**

Entrez les commandes suivantes:

```
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
```

Vous êtes maintenant prêt à démarrer le projet 
```
php artisan serve
```

Dans un navigateur, allez à l'adresse **localhost:8000**

### Démarrage de la version en Laravel et VUE 

Si vous désirez le démo utilisant VUE: 
```
git checkout demoVueCRUD
```

Suivre les étapes pour l'installation purement en Laravel. 

Vous devez avoir NPM installé. Si ce n'est pas fait, suivre les instructions sur https://www.npmjs.com/get-npm

Avant la version 6 de Laravel, VUE était automatiquement installé. Il faut maintenant entrer les commandes suivantes pour l'installer:
```	
composer require laravel/ui --dev
php artisan ui bootstrap
php artisan ui vue 
 
npm install 
npm run dev 
```



## Fonctionnalités
Fonctionnalités déjà réalisées:
* Démo CRUD relation 1 à plusieurs en Laravel et en VUE.js (animaux vs espèce)
* Démo CRUD relation plusieurs à plusieurs en Laravel et en VUE.js (propriétaires vs animaux)
 
Liste de Todo :
* Démo CRUD simple sans relation (espèce)
* Ajouter les try-catch.

## Status
Project est: _in progress_


## Contact
Créé par [@BenoitDesrosiers](https://gitlab.com/BenoitDesrosiers) - contactez moi!
