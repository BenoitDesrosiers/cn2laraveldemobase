@extends('layout')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1> Liste des propriétaires</h1>
                        {{ Form::open(['action'=> ['ProprietairesController@create'], 'role' => 'form', 'method' => 'get', 'class' => 'form-inline']) }}
                            <div class="form-group">
                                <div>
                                    {{ Form::submit('Créer un proprietaire', ['class' => 'btn btn-primary'])}}
                                </div>
                            </div>
                        {{ Form::close() }}
                        @if ($proprietaires->isEmpty())
                            <p> Rien à lister.</p>
                        @else
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th>Téléphone</th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($proprietaires as $proprietaire)
                                    <tr>
                                        <td><a href ="{{ action('ProprietairesController@show', [$proprietaire->id]) }}"> {{ $proprietaire->id }}</a> </td>
                                        <td>{{ $proprietaire->nom }}</td>
                                        <td>{{ $proprietaire->telephone }}</td>
                                        <td><a href="{{ action('ProprietairesController@edit', [$proprietaire->id]) }}" class="btn btn-info">Éditer</a></td>
                                        <td>
                                            {{-- Exemple d'un popup de confirmation avant d'effacer
                                                 Le code JS est dans public/js/script.js, qui est inclue dans layout.blade.php --}}
                                            {{ Form::open(array('action' => array('ProprietairesController@destroy', $proprietaire->id), 'method' => 'delete', 'data-confirm' => 'Êtes-vous certain?')) }}
                                                <button type="submit" href="{{ URL::route('proprietaires.destroy', $proprietaire->id) }}" class="btn btn-danger btn-mini">Effacer</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>


@stop
