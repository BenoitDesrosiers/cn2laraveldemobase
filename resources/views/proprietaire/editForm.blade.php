<?php
use App\Models\Proprietaire;
if(!isset($proprietaire)) {$proprietaire = new proprietaire;}
?>


<div class="form-group">
    {!! Form::label('nom', 'Nom:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('nom', $proprietaire->nom, ['class' => 'form-control']) !!}
        {{ $errors->first('nom') }}
    </div>
</div>

<div class="form-group">
    {!! Form::label('telephone', 'Téléphone:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('telephone', $proprietaire->telephone, ['class' => 'form-control']) !!}
        {{ $errors->first('telephone') }}
    </div>
</div>

<div id="animauxSelect" class="form-group">
    {!!Form::label('dummy', "Animaux:", ['class' =>"col-sm-2 control-label"])!!}
    <div class = 'col-sm-7'>
        {{-- Exemple d'un select multiple
            animaux_id[] : la liste d'id selectionnés qui sera retourné
            $animaux : nom et id des animaux à lister (tous les animaux)
            $proprietaire->animaux->lists('id')->all() : les animaux présentement associés à ce proprio (valeur par défaut)
            'id' : la table contenant les id à utiliser
            'size' : le nombre d'entrés à afficher
            'multiple' : si true, alors permet de faire un select multiple
            'class' : classe CSS pour l'affichage.

        --}}
        {{ Form::select('animaux_id[]', $animaux, $proprietaire->animaux->pluck('id') ,
                ['id' => 'animaux_id', 'size' => 5, 'multiple'=> 'true', 'class' => 'form-control']) }}
    </div>
</div>


