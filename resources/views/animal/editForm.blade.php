<?php
use App\Models\Animal;

if(!isset($animal)) {
    $animal = new animal;
    $especeId = $especes->keys()->first();
} else {
    $especeId = $animal->espece->id;
}
?>


<div class="form-group">
    {!! Form::label('nom', 'Nom:', ['class' => "col-sm-2 control-label"]) !!}
    <div class = 'col-sm-10'>
        {!! Form::text('nom', $animal->nom, ['class' => 'form-control']) !!}
        {{ $errors->first('nom') }}
    </div>


</div>

<div id="especesSelect" class="form-group">
    {!!Form::label('dummy', "Espece:", ['class' =>"col-sm-2 control-label"])!!}
    <div class = 'col-sm-7'>
        {{ Form::select('espece_id', $especes, $especeId, ['class' => 'form-control']) }}
    </div>
</div>






