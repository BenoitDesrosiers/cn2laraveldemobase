@extends('layout')
@section('content')

    <div class="container">
        <section class="section-padding">
            <div class="jumbotron text-left">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1> Liste des animaux</h1>
                        {{ Form::open(['action'=> ['AnimauxController@create'], 'role' => 'form', 'method' => 'get', 'class' => 'form-inline']) }}
                            <div class="form-group">
                                <div>
                                    {{ Form::submit('Créer un animal', ['class' => 'btn btn-primary'])}}
                                </div>
                            </div>
                        {{ Form::close() }}
                        @if ($animaux->isEmpty())
                            <p> Rien à lister.</p>
                        @else
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th></th>
                                    <th></th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($animaux as $animal)
                                    <tr>
                                        <td><a href ="{{ action('AnimauxController@show', [$animal->id]) }}"> {{ $animal->id }}</a> </td>
                                        <td>{{ $animal->nom }}</td>
                                        <td>{{ $animal->telephone }}</td>
                                        <td><a href="{{ action('AnimauxController@edit', [$animal->id]) }}" class="btn btn-info">Éditer</a></td>
                                        <td>
                                            {{-- Exemple d'un popup de confirmation avant d'effacer
                                                 Le code JS est dans public/js/script.js, qui est inclue dans layout.blade.php --}}
                                            {{ Form::open(array('action' => array('AnimauxController@destroy', $animal->id), 'method' => 'delete', 'data-confirm' => 'Êtes-vous certain?')) }}
                                                <button type="submit" href="{{ URL::route('animaux.destroy', $animal->id) }}" class="btn btn-danger btn-mini">Effacer</button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>


@stop
