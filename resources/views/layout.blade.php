<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Demo Cn2 Laravel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery.js"></script>

    <!-- jquery ui -->
    <!--  script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script-->

    <!-- bootstrap -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


    <!-- jquery ui for resizable -->
    <script type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <script type="text/javascript"
            src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css"
          href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css"/>

    {!! Html::script('js/script.js') !!}
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">


</head>
<body >
<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-principal">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <p class="navbar-text navbar-left">Cégep de Drummondville</p>


        </div>
        <div class="collapse navbar-collapse" id="menu-principal">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{!! action('ProprietairesController@index') !!}">Proprio</a></li>
                <li><a href="{!! action('AnimauxController@index') !!}">Animaux</a></li>



            </ul>
        </div>
    </nav>
</header>

    @yield('content')


</body>



</html>
