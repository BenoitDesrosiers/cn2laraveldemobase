<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAnimal;
use App\Models\Espece;
use Illuminate\Http\Request;

use App\Models\Animal;

class AnimauxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $animaux = Animal::all();
        return view('animal.index', compact('animaux'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $especes = Espece::pluck('nom', 'id');
        return view('animal.create', compact('especes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //Exemple d'une route protégée à l'aide d'un request
    public function store(StoreAnimal $request)
    {
        $a = new Animal();
        $a->nom = $request->nom;

        $e = Espece::find($request->espece_id);
        $a->espece()->associate($e);
        $a->save();

        return redirect('animaux');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $animal = Animal::find($id);//TODO ajouter une validation
        $especes = collect([$animal->espece->id => $animal->espece->nom]) ; //bien qu'il n'y en ai qu'un, on retourne ca dans une liste pour l'affichage
        return view('animal.show', compact('animal', 'especes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $animal = Animal::find($id); //TODO ajouter une validation
        $especes = Espece::pluck('nom', 'id');

        return view('animal.edit', compact('animal', 'especes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAnimal $request, $id)
    {
        $a = Animal::find($id);//TODO ajouter une validation
        $a->nom = $request->nom;
        $e = Espece::find($request->espece_id);
        $a->espece()->associate($e);
        $a->save();

        return redirect('animaux');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $a = Animal::find($id);//TODO ajouter une validation
        $a->delete();

        return redirect('animaux');
    }
}
