<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProprietaire;
use Illuminate\Http\Request;

use App\Models\Proprietaire;
use App\Models\Animal;

class ProprietairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $proprietaires = Proprietaire::all();
        return view('proprietaire.index', compact('proprietaires'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $animaux = Animal::pluck('nom', 'id');

        return view('proprietaire.create', compact('animaux'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    //Exemple d'une route protégée à l'aide d'un request
    public function store(StoreProprietaire $request)
    {
        $p = new Proprietaire();
        $p->nom = $request->nom;
        $p->telephone = $request->telephone;
        $p->save();
        $p->animaux()->sync($request->animaux_id);

        return redirect('proprietaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proprietaire = Proprietaire::find($id);//TODO ajouter une validation
        $animaux = $proprietaire->animaux()->pluck('nom', 'id');
        return view('proprietaire.show', compact('proprietaire', 'animaux'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proprietaire = Proprietaire::find($id); //TODO ajouter une validation
        $animaux = Animal::pluck('nom', 'id');
        return view('proprietaire.edit', compact('proprietaire','animaux'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProprietaire $request, $id)
    {
        $p = Proprietaire::find($id);//TODO ajouter une validation
        $p->nom = $request->nom;
        $p->telephone = $request->telephone;
        $p->save();
        $p->animaux()->sync($request->animaux_id);
        return redirect('proprietaires');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $p = Proprietaire::find($id);//TODO ajouter une validation
        $p->delete();

        return redirect('proprietaires');
    }
}
