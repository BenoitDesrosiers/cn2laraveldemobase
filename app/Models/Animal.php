<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    //Exemple d'un nom de table ne respectant pas la convention (animaux au lieu de animals)
    protected $table = 'animaux';
    protected $guarded = array('id');

    public function proprietaires() {
        //Exemple de spécification du nom de la table ne respectant pas la convention
        //Exemple de spécification du nom de la foreign key ne respectant pas la convention (animal_id au lieu de animaux_id)
        return $this->belongsToMany('App\Models\Proprietaire','animaux_proprietaires', 'animal_id');
    }

    public function espece() {
        return $this->belongsTo('App\Models\Espece');
    }
}
