<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Espece extends Model
{
    /*
     *
     * Mass assignment protection
     */
    protected $guarded = array('id');

    public function animaux() {
        return $this->hasMany('App\Models\Animal');
    }

}
