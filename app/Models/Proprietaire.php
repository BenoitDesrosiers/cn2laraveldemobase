<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proprietaire extends Model
{
    protected $guarded = array('id');

    public function animaux() {
        //Exemple de nom de table et de relatedPivotKey ne respectant pas la convention
        return $this->belongsToMany('App\Models\Animal','animaux_proprietaires',null,'animal_id');
    }
}
